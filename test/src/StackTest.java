import static org.junit.Assert.*;
import junit.framework.TestCase;
import model.data_structures.Stack;

import org.junit.Test;


public class StackTest extends TestCase{

	private Stack<String> pila;


	//---------------------------------------------------------------------------------//
	//---------------------------------SETUP_ESCENARIOS--------------------------------//
	//---------------------------------------------------------------------------------//

	/**
	 * Crea una pila vacia. 
	 */

	public void setupEscenario1(){
		pila = new Stack<>();

	}


	/**
	 * Crea una pila y agrega un elemento 
	 */

	public void setupEscenario2(){
		pila = new Stack<>();

		pila.push("soy una cadena xDXdXdxXdXd");

	}

	/**
	 * Crea una pila y agrega cuatro elementos
	 */

	public void setupEscenario3(){
		pila = new Stack<>();

		pila.push("No vamos a hacer la primera entrega a tiempo");
		pila.push("No c, weno zi c pero no te wa a dezir");
		pila.push("Ayy, muchas cosas wohooo");
		pila.push("Yo necisito amor, comrpensi�n y ternura");
	}

	//---------------------------------------------------------------------------------//
	//---------------------------------M�TODOS_PRUEBAS--------------------------------//
	//---------------------------------------------------------------------------------//

	/**
	 * Prueba el m�todo que retorna el tama�o de las listas
	 */

	public void testSize(){
		setupEscenario1();
		assertEquals("El tama�o de la pila deber�a ser 0",0, pila.size());

		setupEscenario2();
		assertEquals("El tama�o de la pila deberia ser 1", 1, pila.size());

		setupEscenario3();
		assertEquals("E� tama�o de la pila deber�a ser 4", 4, pila.size());

	}

	/**
	 * Prueba el m�todo que informa si las pilas estan vacias o no
	 */

	public void testIsEmpty(){
		setupEscenario1();
		assertEquals("La pila deber�a estar vacia",true, pila.isEmpty());

		setupEscenario2();
		assertEquals("La pila no deber�a estar vacia",false, pila.isEmpty());
		pila.pop();
		assertEquals("La pila deber�a estar vac�a ahora",true, pila.isEmpty());

		setupEscenario3();
		assertEquals("La pila no deber�a estar vacia",false, pila.isEmpty());
	}

	/**
	 * Prueba el m�todo que atienden al ultimo elemento que lleg� a la pila
	 */

	public void testPop(){
		setupEscenario1();
		try{
			pila.pop();
			fail();
		} catch(Exception e){

		}

		setupEscenario2();
		assertEquals("El elemento atendido de la pila no corresponde con el esperado", "soy una cadena xDXdXdxXdXd", pila.pop());
		
		setupEscenario3();
		assertEquals("El elemento atendido de la pila no corresponde con el esperado", "Yo necisito amor, comrpensi�n y ternura", pila.pop());
		assertEquals("El elemento atendido de la pila no corresponde con el esperado", "Ayy, muchas cosas wohooo", pila.pop());
		assertEquals("El elemento atendido de la pila no corresponde con el esperado", "No c, weno zi c pero no te wa a dezir", pila.pop());
		assertEquals("El elemento atendido de la pila no corresponde con el esperado", "No vamos a hacer la primera entrega a tiempo", pila.pop());
	}
	
	/**
	 * Prueba el m�todo que agrega elementos a la pila
	 */
	
	public void testPush(){
		setupEscenario1();	
		pila.push("Holi");
		assertEquals("El elemento agregado a la pila no corresponde con el esperado", "Holi", pila.pop());
		
		setupEscenario2();
		pila.push("Esta no es otra tipica cadena");
		pila.push("100% real no feik");
		assertEquals("El elemento agregado a la pila no corresponde con el esperado", "100% real no feik", pila.pop());
		assertEquals("El elemento agregado a la pila no corresponde con el esperado", "Esta no es otra tipica cadena", pila.pop());
		
		
	}



}
