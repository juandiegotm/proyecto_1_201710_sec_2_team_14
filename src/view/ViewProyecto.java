package view;
import java.util.Scanner;

import model.*;
import model.data_structures.ListaDobleEncadenada;
import model.logic.SistemaRecomendacionPeliculas;
import model.vo.VOGeneroUsuario;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;

public class ViewProyecto {

	public static void main(String[] args) {

		final String RUTA_PELICULAS = "./data/movies.csv";
		final String RUTA_TAGS = "./data/tags.csv";
		final String RUTA_ratings = "./data/ratings.csv";
		final String RUTA_RECOMENDACIONES = "/.data/ratings.csv";
		
		SistemaRecomendacionPeliculas sr = new SistemaRecomendacionPeliculas();

		while(true){
			printMenu();
			Scanner sc = new Scanner(System.in);
			int seleccion = sc.nextInt();

			switch(seleccion){

			case 1: 
				sr.cargarPeliculasSR(RUTA_PELICULAS);
				sr.cargarRatingsSR(RUTA_ratings);
				sr.cargarTagsSR(RUTA_TAGS);
				break;

			case 6:
				System.out.println("Ingrese el n�mero de usuarios: " );
				int usuariosMasActivos = sc.nextInt();
				ListaDobleEncadenada<VOGeneroUsuario> listaUsuariosMasActivos = (ListaDobleEncadenada<VOGeneroUsuario>)sr.usuariosActivosSR(usuariosMasActivos);
				for(VOGeneroUsuario vgu : listaUsuariosMasActivos){
					System.out.println("************************************");
					System.out.println(vgu.getGenero());
					System.out.println("************************************");
					ListaDobleEncadenada<VOUsuarioConteo> listaUsuarios = (ListaDobleEncadenada<VOUsuarioConteo>) vgu.getUsuarios();
					for(VOUsuarioConteo vgc: listaUsuarios){
						System.out.println("IDUsuario: " + vgc.getIdUsuario() + " Conteo: " + vgc.getConteo() );
					}
					System.out.println("************************************");
				}
			}


		}
	}
	public static void printMenu(){

		System.out.println("****************************************************");
		System.out.println("******Proyecto 1 - Estructuras de Datos 201710******");
		System.out.println("****************************************************");
		System.out.println("Seleccione una de las siguientes opciones: ");
		System.out.println("1. Cargar todos los archivo del sistemas de recomendaci�n");
		System.out.println("********************PARTE A**************************");
		System.out.println("2. ");
		System.out.println("********************PARTE B**************************");
		System.out.println("6. Dar los n usuarios m�s activos");
	}



}
