package model.logic;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.security.Timestamp;
import java.util.Iterator;

import com.sun.org.apache.bcel.internal.generic.DASTORE;

import java.util.Comparator;
import java.util.Iterator;


import api.ISistemaRecomendacionPeliculas;
import model.Comparator.ComparadorVOPeliculaPorCantidadTags;
import model.Comparator.ComparatorString;
import model.Comparator.ComparatorVOPeliculaPorAgno;
import model.Comparator.ComparatorVOPeliculaPorIDPelicula;
import model.Comparator.ComparatorVOPeliculaPorPromCalificacion;
import model.Comparator.ComparatorVOPeliculasGeneral;
import model.Comparator.ComparatorVOPeliporPopularidad;
import model.Comparator.ComparatorVORatingPorIdPeliculaIdUser;
import model.Comparator.ComparatorVORatingPorIdUserIdPeli;
import model.Comparator.ComparatorVORatingPorTimestamp;
import model.Comparator.ComparatorVORatingsPorIdPeliculas;
import model.Comparator.ComparatorVOTagIdUsuarioIdPelicula;
import model.Comparator.ComparatorVOTagsExtendidoPorIDPelicula;
import model.Comparator.ComparatorVOTagsPorIDPelicula;
import model.Comparator.ComparatorVOUsuarioConteoConteo;
import model.Comparator.ComparatorVOUsuarioPorDiferenciaOpinion;
import model.Comparator.ComparatorVOUsuarioPorIdPelicula;
import model.Comparator.ComparatorVOUsuarioPorTimestampNumeroRatingsId;
import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOTagExtendido;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas {

	private ListaDobleEncadenada<VOPelicula> listaPeliculas; 
	private ListaDobleEncadenada<VOUsuario> usuarios;
	private ListaDobleEncadenada<VORating> ratings;
	private ListaDobleEncadenada<String> nombresGeneros;
	private ListaDobleEncadenada<VOTag> tags;
	private ListaDobleEncadenada<VOOperacion> operaciones;

	public SistemaRecomendacionPeliculas() {
		// TODO Auto-generated constructor stub
		operaciones = new ListaDobleEncadenada<VOOperacion>();
	}
	@Override

	/**
	 * Carga las peliculas y las deja en el orden por id.\n
	 * Carga una lista con los nombres de todos los generos existentes.
	 */
	public boolean cargarPeliculasSR(String rutaPeliculas){

		long inicio = -System.currentTimeMillis();


		// TODO Auto-generated method stub
		boolean peliculasCargadas=false;
		try{
			File archivoCSV = new File(rutaPeliculas);
			BufferedReader lector = new BufferedReader(new FileReader(archivoCSV));
			listaPeliculas = new ListaDobleEncadenada<VOPelicula>();
			nombresGeneros = new ListaDobleEncadenada<String>();
			//peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();

			String linea = lector.readLine();
			linea = lector.readLine();

			while(linea != null){
				// Recosntrucci�n del titulo de la pelicula

				String [] info = linea.split(",");
				String auxTitulo = "";


				for(int i = 1; i < info.length -1; i++)
					auxTitulo+= info[i] + ",";

				auxTitulo = auxTitulo.substring(0, auxTitulo.length()-1);
				if(auxTitulo.charAt(0) == '"')
					auxTitulo = auxTitulo.substring(1, auxTitulo.length() - 1);

				auxTitulo = auxTitulo.trim();


				// Obtención fecha de la pelicula	
				String fechaEnCadena = "";
				int fecha = 0;
				boolean tieneFecha = false;

				if( auxTitulo.charAt(auxTitulo.length()-1) == ')'){

					tieneFecha = true;
					fechaEnCadena = auxTitulo.substring(auxTitulo.length()-5, auxTitulo.length()-1 );
					for(int i = 0; i< fechaEnCadena.length() && tieneFecha; i++){
						Character caracterActual = fechaEnCadena.charAt(i);
						if(!Character.isDigit(caracterActual))
							tieneFecha = false;
					}
				}

				//Si la pelicula tiene fecha, se le quita del titulo

				String nombre = auxTitulo;
				if(tieneFecha){
					nombre = auxTitulo.substring(0,auxTitulo.length()-6);
					fecha = Integer.parseInt(fechaEnCadena);
				}

				//Obtenci�n de los generos
				String aux[] = info[info.length-1].split("\\|");

				ListaDobleEncadenada<String> generos = new ListaDobleEncadenada<String>(); 
				for(int i = 0; i < aux.length; i++)
					generos.agregarElementoFinal(aux[i]);


				//Creaci�n de la pelicula y asignaci�n de sus atributos
				VOPelicula peliculaAAgregar = new VOPelicula();
				peliculaAAgregar.setAgnoPublicacion(fecha);
				peliculaAAgregar.setTitulo(nombre);
				peliculaAAgregar.setGenerosAsociados(generos);
				peliculaAAgregar.setIdUsuario(Long.parseLong(info[0]));
				peliculaAAgregar.setTagsAsociados(new ListaDobleEncadenada<String>());

				//A�adiendo las peliculas a la lista en desorden
				listaPeliculas.agregarElementoFinal(peliculaAAgregar);

				//Agrega los generos de la pelicula a la lista de nombres de generos.
				for(String e: generos)
					nombresGeneros.agregarElementoFinal(e);

				linea = lector.readLine();
			}

			lector.close();
			peliculasCargadas = true;

			// ordena los generos eliminando los repetidos.

			nombresGeneros.sort(new ComparatorString());
			nombresGeneros.eliminarRepetidos(new ComparatorString());

		}
		catch(Exception e){

		}


		long fin = inicio + System.currentTimeMillis();
		agregarOperacionSR("cargarPeliculas", inicio, fin);

		return peliculasCargadas;
	}


	@Override
	public boolean cargarRatingsSR(String rutaRatings) {

		long inicio = -System.currentTimeMillis();


		// TODO Auto-generated method stub
		boolean ratingCargados = false;
		try{
			File archivo = new File(rutaRatings);
			BufferedReader reader = new BufferedReader(new FileReader(archivo));
			ratings = new ListaDobleEncadenada<>();
			usuarios = new ListaDobleEncadenada<>();

			String linea = reader.readLine();
			linea = reader.readLine();

			//Varibles necesarias 
			int idUserActual = 1;
			int cantidadRatings = 0;
			long primerTimestamp = Long.MAX_VALUE;
			double sumaCalificaciones = 0
					;
			while(linea != null && linea != ""){

				String [] Info = linea.split(",");

				int idUser = Integer.parseInt(Info[0]);

				//Si el usario sigue siendo lo mismo, se crea un nuevo rating pero
				//la info del usuario se acumula

				if(idUserActual == idUser){
					cantidadRatings++;
					int movieId = Integer.parseInt(Info[1]);
					double calificacion = Double.parseDouble(Info[2]);
					sumaCalificaciones += calificacion;
					long timestampActual = Long.parseLong(Info[3]);

					//Busca el primer timestamp que es el primero 
					if(timestampActual < primerTimestamp){
						primerTimestamp = Long.parseLong(Info[3]);
					}

					VORating rating = new VORating();
					rating.setIdPelicula(movieId);
					rating.setIdUsuario(idUser);
					rating.setRating(calificacion);
					rating.setTimestamp(timestampActual);
					ratings.agregarElementoFinal(rating);

					linea = reader.readLine();
				}

				//Si es diferente, se agregar al usuario leido y se reinician los valores
				else{
					VOUsuario usuario = new VOUsuario();

					usuario.setIdUsuario(idUserActual);
					usuario.setNumRatings(cantidadRatings);
					usuario.setPrimerTimestamp(primerTimestamp);
					double diferencia = sumaCalificaciones/cantidadRatings;
					usuario.setDiferenciaOpinion(diferencia);

					usuarios.agregarElementoFinal(usuario);

					idUserActual = idUser;
					sumaCalificaciones = 0;
					cantidadRatings = 0;	
					primerTimestamp = Long.MAX_VALUE; 
				}
			}

			//Agregar ratings a peliculas
			ratings.sort(new ComparatorVORatingsPorIdPeliculas());


			int numeroRatings = 0;
			double sumaRatings = 0.0;

			for(VOPelicula peli: listaPeliculas){
				VORating ratingActual = ratings.darElementoPosicionActual();

				while(ratings.avanzarSiguientePosicion() && ratingActual.getIdPelicula() == peli.getIdUsuario()){
					ratingActual = ratings.darElementoPosicionActual();
					numeroRatings++;
					sumaRatings += ratingActual.getRating();

				}


				if(peli.getIdUsuario() != ratingActual.getIdPelicula()){
					//Asigna las calificaciones a la pelicula actual
					peli.setNumeroRatings(numeroRatings);
					double promedio = numeroRatings!= 0 ? sumaRatings/numeroRatings: 0;
					peli.setPromedioRatings(promedio);

					//Reinicia los valores
					numeroRatings = 0;
					sumaRatings = 0.0;
				}
			}

			listaPeliculas.restaurarActual();
			ratingCargados = true;
		}

		catch(Exception e){

		}


		long fin = inicio + System.currentTimeMillis();
		agregarOperacionSR("cargarRatings", inicio, fin);

		return ratingCargados;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {

		long inicio = -System.currentTimeMillis();


		boolean tagsCargados = false;
		try{
			File archivo = new File(rutaTags);
			BufferedReader lector = new BufferedReader(new FileReader(archivo));
			ListaDobleEncadenada<VOTagExtendido> tagsAux = new ListaDobleEncadenada<>();
			tags = new ListaDobleEncadenada<>();

			lector.readLine();
			String linea = lector.readLine();

			//Agregar todos los TAgs a una lista de tags auxiliar

			while(linea != null )
			{

				String [] info = linea.split(",");
				String etiqueta = "";

				//Es posible que la etiquta tenga comas, por eso se
				//reconstruye

				if(info.length >= 5){
					for(int i = 3; i < info.length-2; i++)
						etiqueta += info[i] + "";
					etiqueta = etiqueta.substring(1, etiqueta.length()-2);
				}

				else
					etiqueta = info[2];




				long idUsuario = Long.parseLong(info[0]);
				long idPelicula = Long.parseLong(info[1]);

				long timestamp = Long.parseLong(info[info.length-1]);



				//Creamos tags extendido para ordenar esta lista por IdPelicula
				//y agregarle los tags a cada pelicula de forma eficiente
				VOTagExtendido tagExtendido = new VOTagExtendido();
				tagExtendido.setIdPelicula(idPelicula);
				tagExtendido.setIdUsuario(idUsuario);
				tagExtendido.setTag(etiqueta);
				tagExtendido.setTimestamp(timestamp);
				tagsAux.agregarElementoFinal(tagExtendido);

				// Se crea la lista de tags que quedará permanentemente en memoria
				VOTag tag = new VOTag();
				tag.setIdUsuario(idUsuario);
				tag.setIdPelicula(idPelicula);
				tag.setTag(etiqueta);
				tag.setTimestamp(timestamp);
				tags.agregarElementoFinal(tag);

				linea = lector.readLine();
			}


			//Ordenar lista Aux y agregarla a las peliculas
			tagsAux.sort(new ComparatorVOTagsExtendidoPorIDPelicula());
			listaPeliculas.restaurarActual();



			for(VOTagExtendido tagAct : tagsAux)
			{
				VOPelicula peli = listaPeliculas.darElementoPosicionActual();

				while(tagAct.getIdPelicula() != peli.getIdUsuario() && listaPeliculas.avanzarSiguientePosicion())
					peli = listaPeliculas.darElementoPosicionActual();

				if(peli.getIdUsuario() == tagAct.getIdPelicula()){
					peli.getTagsAsociados().agregarElementoFinal(tagAct.getTag());
					peli.setNumeroTags(peli.getNumeroTags()+ 1);
				}
			}


			lector.close();
			tagsCargados = true;	
		}

		catch(Exception e){
			e.printStackTrace();
		}


		long fin = inicio + System.currentTimeMillis();
		agregarOperacionSR("cargarTags", inicio, fin);

		return tagsCargados;
	}

	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return listaPeliculas.darNumeroElementos();
	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return usuarios.darNumeroElementos();
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return tags.darNumeroElementos();
	}

	//-----------------------------------------------------------------------------//
	//----------------------------PARTE A------------------------------------------//
	//-----------------------------------------------------------------------------//

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {

		long inicio = -System.currentTimeMillis();

		ILista<VOGeneroPelicula> rta = GeneroPeliculaPorCriterio(new ComparatorVOPeliporPopularidad(), n);

		long fin = inicio + System.currentTimeMillis();
		agregarOperacionSR("ratingsPeliculas", inicio, fin);

		return rta;
	}


	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {

		long inicio = -System.currentTimeMillis();

		listaPeliculas.sort(new ComparatorVOPeliculasGeneral());

		long fin = inicio + System.currentTimeMillis();
		agregarOperacionSR("catalogoPeliculasOrdenado", inicio, fin);

		return listaPeliculas;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {

		long inicio = -System.currentTimeMillis();
		// ordenar peliculas por Promedio calificacion.
		listaPeliculas.sort(new ComparatorVOPeliculaPorPromCalificacion());		

		//  crea una lista de VOGeneroPelicula�s. cada uno con lista de peliculas vacia.
		ListaDobleEncadenada<VOGeneroPelicula> pelisPorGenero = new ListaDobleEncadenada<VOGeneroPelicula>();
		for(String g: nombresGeneros){
			VOGeneroPelicula nuevoGenero = new VOGeneroPelicula();
			nuevoGenero.setGenero(g);
			nuevoGenero.setPeliculas(new ListaDobleEncadenada<VOPelicula>());
			pelisPorGenero.agregarElementoFinal(nuevoGenero);
		}


		// recorre las peliculas y las agrega a las listas de los respectivos generos que tiene.
		for(VOPelicula peli: listaPeliculas){
			ListaDobleEncadenada<String> gensAsociados = (ListaDobleEncadenada<String>) peli.getGenerosAsociados();
			gensAsociados.sort(new ComparatorString());

			Iterator<String> iGensAsociados=gensAsociados.iterator();
			Iterator<VOGeneroPelicula> iVOGen = pelisPorGenero.iterator();

			String genAsociadoActual = iGensAsociados.next();

			//recorre los VOGeneros y agrega la pelicula actual si coinciden los generos y no se a completado el max. 
			while(iVOGen.hasNext() && iGensAsociados !=null){

				VOGeneroPelicula VOGenAct = iVOGen.next();

				if(VOGenAct.getPeliculas().darNumeroElementos()==0){
					VOGenAct.setMejorCalificacion(peli.getPromedioRatings());
					VOGenAct.getPeliculas().agregarElementoFinal(peli);
					genAsociadoActual = iGensAsociados.hasNext()?iGensAsociados.next():null;
				}	
				else if(peli.getPromedioRatings()==VOGenAct.getMejorCalificacion() && VOGenAct.getGenero().equals(genAsociadoActual)){
					VOGenAct.getPeliculas().agregarElementoFinal(peli);
					genAsociadoActual = iGensAsociados.hasNext()?iGensAsociados.next():null;
				}
			}



			long fin = inicio + System.currentTimeMillis();
			agregarOperacionSR("recomendarGeneros", inicio, fin);
		}	
		return pelisPorGenero;
	}

	private ILista<VOGeneroPelicula> GeneroPeliculaPorCriterio(Comparator<VOPelicula> criterio, int maxPelis) {
		// ordenar peliculas por el el criterio dado por parametro.
		System.out.println("Imprimo");
		listaPeliculas.sort(criterio);
		System.out.println("Imprimo");

		//  crea una lista de VOGeneroPelicula�s. cada uno con lista de peliculas vacia.
		ListaDobleEncadenada<VOGeneroPelicula> pelisPorGenero = new ListaDobleEncadenada<VOGeneroPelicula>();
		for(String g: nombresGeneros){

			VOGeneroPelicula nuevoGenero = new VOGeneroPelicula();
			nuevoGenero.setGenero(g);
			nuevoGenero.setPeliculas(new ListaDobleEncadenada<VOPelicula>());
			nuevoGenero.setMaxPelis(maxPelis);
			pelisPorGenero.agregarElementoFinal(nuevoGenero);
		}


		// recorre las peliculas y las agrega a las listas de los respectivos generos que tiene.
		for(VOPelicula peli: listaPeliculas){
			ListaDobleEncadenada<String> gensAsociados = (ListaDobleEncadenada<String>) peli.getGenerosAsociados();
			gensAsociados.sort(new ComparatorString());

			Iterator<String> iGensAsociados=gensAsociados.iterator();
			Iterator<VOGeneroPelicula> iVOGen = pelisPorGenero.iterator();

			String genAsociadoActual = iGensAsociados.next();

			//recorre los VOGeneros y agrega la pelicula actual si coinciden los generos y no se a completado el max. 
			while(iVOGen.hasNext() && iGensAsociados !=null){

				VOGeneroPelicula VOGenAct = iVOGen.next();

				if(VOGenAct.getMaxPelis()>0 && VOGenAct.getGenero().equals(genAsociadoActual)){
					VOGenAct.getPeliculas().agregarElementoFinal(peli);
					VOGenAct.setMaxPelis(VOGenAct.getMaxPelis()-1);
					genAsociadoActual = iGensAsociados.hasNext()?iGensAsociados.next():null;
				}
			}

		}
		return pelisPorGenero;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		long inicio = -System.currentTimeMillis();

		ILista<VOGeneroPelicula> rta = GeneroPeliculaPorCriterio(new ComparatorVOPeliculaPorAgno(), listaPeliculas.darNumeroElementos());

		long fin = inicio + System.currentTimeMillis();
		agregarOperacionSR("opinionRatingsGenero", inicio, fin);

		return rta;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {

		long inicio = -System.currentTimeMillis(); 

		ListaDobleEncadenada<VOPeliculaPelicula> PPRecomendacion = new ListaDobleEncadenada<>(); 
		listaPeliculas.sort(new ComparatorVOPeliculaPorIDPelicula());
		// Crea Lista de Peliculas por genero para buscar las peliculas relacionadas.
		ListaDobleEncadenada<VOGeneroPelicula > peliculasPorGenero =  (ListaDobleEncadenada<VOGeneroPelicula>) GeneroPeliculaPorCriterio(new ComparatorVOPeliculaPorPromCalificacion(), listaPeliculas.darNumeroElementos());

		try{
			// Carga las id y la calificacion dada de las peliculas vistas m�s recientemente
			File archivoCSV = new File(rutaRecomendacion);
			BufferedReader lector = new BufferedReader(new FileReader(archivoCSV));
			String linea = lector.readLine();
			linea = lector.readLine();

			//Crea lista de idPelicula con Calificacion
			Queue<String[]> sVistas =new Queue<String[]>();
			for(;linea != null && n>0; n--){
				String[] inf = linea.split(",");

				//Pone la VOPelicula en fila.
				sVistas.enqueue(inf);
			}
			lector.close();

			while(!sVistas.isEmpty()){
				// Va atendiendo la fila de peliculas vistas por el usuario 
				// y agrega la peli vista y sus asociadas a la lista respuesta
				VOPeliculaPelicula asociadasPAct=new VOPeliculaPelicula();

				String[] peliVistaAct = sVistas.dequeue();
				long idPeliVista = Long.parseLong(peliVistaAct[0]);
				double calificacion = Double.parseDouble(peliVistaAct[1]);				

				Iterator<VOGeneroPelicula> iteGeneros = peliculasPorGenero.iterator();
				boolean esLaPeli = false;
				while(iteGeneros.hasNext() && !esLaPeli ){
					//Recorre los Generos hasta encontrar tanto la pelicula vista como las asociadas.
					VOGeneroPelicula generoAct = iteGeneros.next();
					ListaDobleEncadenada<VOPelicula> pelisGeneroActual = (ListaDobleEncadenada<VOPelicula>) generoAct.getPeliculas();
					pelisGeneroActual.restaurarActual();
					VOPelicula peliActual = pelisGeneroActual.darElementoPosicionActual();
					while(peliActual!=null && !esLaPeli){
						//Recorre las peliculas del genero actual hasta encontrar la peli vista y sus asociadas.

						if(peliActual.getIdUsuario()==idPeliVista && peliActual.getGenerosAsociados().darElemento(0).compareTo(generoAct.getGenero())==0){
							// Si la pelicula tiene el id de la plei vista comprueba que el genero principal coincida con
							//el genero actual.
							esLaPeli=true;

							// Agrega Las peliculas relacionadas con diferencia absoluta menor a .5
							boolean subListEnPosicion = false;
							pelisGeneroActual.iniciarNodoAux();
							while(!subListEnPosicion){
								boolean enPosicion1 =false;
								double diferencia1 = (calificacion-pelisGeneroActual.darElementoPosicionActual().getPromedioRatings())*(-1);
								if(diferencia1<=0.5)
									enPosicion1=!pelisGeneroActual.retrocederPosicionAnterior();
								else
									enPosicion1=true;

								boolean enPosicion2 = false;
								double diferencia2 = calificacion-pelisGeneroActual.darElementoAux().getPromedioRatings();
								if(diferencia2 <= 0.5)
									enPosicion2=pelisGeneroActual.nAuxAvanzar();
								else
									enPosicion2=false;

								subListEnPosicion=enPosicion1&&enPosicion2;
							}
							asociadasPAct.setPelicula(peliActual);
							asociadasPAct.setPeliculasRelacionadas(pelisGeneroActual.subListaAux());	
						}
						else{
							//Si no es la peli Avanza
							pelisGeneroActual.avanzarSiguientePosicion();
							peliActual=pelisGeneroActual.darElementoPosicionActual();
						}

					}
				}				
				//Agrega la peliVistaActual con sus asociaciones a la lista PPRecomendaciones que se retorna.
				PPRecomendacion.agregarElementoFinal(asociadasPAct);
			}

		}
		catch(Exception e){}


		long fin = inicio + System.currentTimeMillis();
		agregarOperacionSR("recomendarPeliculas", inicio, fin);

		return PPRecomendacion;
	}

	/**
	 * Busca la pelicula con la id pasada por parametro. Asume que las peliculas estan ordenadas por id;
	 * @param idPeli
	 * @return VOPelicula con la id dada por parametro
	 */
	private VOPelicula buscarPeliPorId(long idPeli) {

		Iterator<VOPelicula> ite = listaPeliculas.iterator();
		VOPelicula rta = ite.next();
		boolean encontrado = false;
		while(rta != null && !encontrado){

			VOPelicula sig = ite.next();

			if(sig.getIdUsuario()<=idPeli)
				rta = sig;
			else
				encontrado=true;
		}
		return rta;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {

		long inicio = -System.currentTimeMillis();

		ratings.sort(new ComparatorVORatingPorTimestamp());
		ListaDobleEncadenada<VORating> rta = new ListaDobleEncadenada<>();
		for(VORating r: ratings)
			if(r.getIdPelicula()==idPelicula)
				rta.agregarElementoFinal(r);



		long fin = inicio + System.currentTimeMillis();
		agregarOperacionSR("ratingsPeliculas", inicio, fin);

		return ratings;
	}

	//-----------------------------------------------------------------------------//
	//----------------------------PARTE B------------------------------------------//
	//-----------------------------------------------------------------------------//

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		// TODO Auto-generated method stub
		long inicio = -System.currentTimeMillis();
		ListaDobleEncadenada<VOGeneroUsuario> usuariosActivosPorGenero = new ListaDobleEncadenada<>();
		//Rellenar lista vacia de generos
		for(String generoGrande: nombresGeneros){
			VOGeneroUsuario genPel = new VOGeneroUsuario();
			genPel.setUsuarios(new ListaDobleEncadenada<VOUsuarioConteo>());
			genPel.setGenero(generoGrande);
			usuariosActivosPorGenero.agregarElementoFinal(genPel);
		}


		//1. Organizar la lista de ratings por usuario y, ahí mismo, organizarla por
		//idPeliculas
		ratings.sort(new ComparatorVORatingPorIdUserIdPeli());
		listaPeliculas.sort(new ComparatorVOPeliculaPorIDPelicula());

		//Recorrer cada registro de rating para cada usuario y guardarlas en los generos
		//correspondientes
		listaPeliculas.restaurarActual();
		ListaDobleEncadenada<VOGeneroUsuario> listaGenerosUsuario= new ListaDobleEncadenada<>();
		long idUserEnConteo = ratings.darElemento(0).getIdUsuario();
		int contador = 0;

		ratings.restaurarActual();
		VORating rating = ratings.darElementoPosicionActual();
		boolean primeraVez = true;

		while(ratings.avanzarSiguientePosicion()){

			//Esto evita que se ignore al primer elemento
			if(primeraVez){
				primeraVez = false;
				ratings.retrocederPosicionAnterior();
			}
			rating = ratings.darElementoPosicionActual();
			long idUserActual = rating.getIdUsuario();
			if(idUserActual == idUserEnConteo){
				VOPelicula pelicula = listaPeliculas.darElementoPosicionActual();
				while(listaPeliculas.avanzarSiguientePosicion()&& rating.getIdPelicula() != pelicula.getIdUsuario()){
					pelicula = listaPeliculas.darElementoPosicionActual();
				}

				if(rating.getIdPelicula() == pelicula.getIdUsuario()){
					ListaDobleEncadenada<String> generos = (ListaDobleEncadenada<String>)pelicula.getGenerosAsociados();

					//No tiene ni un solo registro
					if(listaGenerosUsuario.isEmpty()){
						//Crea una nueva lista de un elemento de un genero
						VOGeneroUsuario nuevaGeneroUsuario = new VOGeneroUsuario();
						VOUsuarioConteo nuevoContador = new VOUsuarioConteo();

						//Crea un nuevo contador para esa lista
						nuevoContador.setIdUsuario(idUserEnConteo);
						nuevoContador.setConteo(1);

						//Le asigna el genero a la lista del genero
						nuevaGeneroUsuario.setGenero(generos.darElemento(0));
						ListaDobleEncadenada<VOUsuarioConteo> conteoUnSoloElmento = new ListaDobleEncadenada<VOUsuarioConteo>();
						conteoUnSoloElmento.agregarElementoFinal(nuevoContador);
						nuevaGeneroUsuario.setUsuarios(conteoUnSoloElmento);

						listaGenerosUsuario.agregarElementoFinal(nuevaGeneroUsuario);

					}
					else{
						VOGeneroUsuario listaGeneroActual = listaGenerosUsuario.darElementoPosicionActual();
						//Se busca el genero en la lista de VOPeliculas
						boolean existeGenero = false;
						listaGenerosUsuario.restaurarActual();
						VOGeneroUsuario generoUsuarioBuscado = null;
						for(String genero: generos){

							String generoActual = listaGenerosUsuario.darElementoPosicionActual().getGenero();
							while(listaGenerosUsuario.avanzarSiguientePosicion() && !existeGenero && !generoActual.equals(genero)){
								generoActual = listaGenerosUsuario.darElementoPosicionActual().getGenero();

							}

							if(generoActual.equals(genero)){
								existeGenero = true;
								generoUsuarioBuscado = listaGenerosUsuario.darElementoPosicionActual();

							}

							if(existeGenero)
							{

								int conteo = generoUsuarioBuscado.getUsuarios().darElemento(0).getConteo();
								generoUsuarioBuscado.getUsuarios().darElemento(0).setConteo(conteo + 1);
							}

							else{

								VOGeneroUsuario nuevaGeneroUsuario = new VOGeneroUsuario();
								VOUsuarioConteo nuevoContador = new VOUsuarioConteo();

								//Crea un nuevo contador para esa lista
								nuevoContador.setIdUsuario(idUserEnConteo);
								nuevoContador.setConteo(1);

								//Le asigna el genero a la lista del genero
								nuevaGeneroUsuario.setGenero(genero);
								ListaDobleEncadenada<VOUsuarioConteo> conteoUnSoloElmento = new ListaDobleEncadenada<VOUsuarioConteo>();
								conteoUnSoloElmento.agregarElementoFinal(nuevoContador);
								nuevaGeneroUsuario.setUsuarios(conteoUnSoloElmento);

								listaGenerosUsuario.agregarElementoFinal(nuevaGeneroUsuario);
							}
						}


					}

				}	

			}


			else{

				idUserEnConteo = idUserActual;
				listaPeliculas.restaurarActual();
				usuariosActivosPorGenero.restaurarActual();
				for(VOGeneroUsuario generoUsuario: listaGenerosUsuario){
					VOGeneroUsuario generoUsuarioGrande = usuariosActivosPorGenero.darElementoPosicionActual();
					while(usuariosActivosPorGenero.avanzarSiguientePosicion()&& 
							!generoUsuarioGrande.getGenero().equals(generoUsuario.getGenero())){
						generoUsuarioGrande = usuariosActivosPorGenero.darElementoPosicionActual();

					}

					if(generoUsuario.getGenero().equals(generoUsuarioGrande.getGenero()))
					{
						generoUsuarioGrande.getUsuarios().agregarElementoFinal(generoUsuario.getUsuarios().darElemento(0));
					}
				}
				listaGenerosUsuario = new ListaDobleEncadenada<>();

				ratings.retrocederPosicionAnterior();

			}

		}

		for(VOGeneroUsuario generoUsuario: usuariosActivosPorGenero){
			ListaDobleEncadenada<VOUsuarioConteo> lista= (ListaDobleEncadenada<VOUsuarioConteo>)generoUsuario.getUsuarios();
			lista.sort(new ComparatorVOUsuarioConteoConteo());

			ListaDobleEncadenada<VOUsuarioConteo> listaDelimitada = new ListaDobleEncadenada<>();;
			int limite =0;
			Iterator it = lista.iterator();
			while(it.hasNext() && limite < n){
				listaDelimitada.agregarElementoFinal((VOUsuarioConteo)it.next());
				limite++;
			}
			generoUsuario.setUsuarios(listaDelimitada);
		}
		long fin = System.currentTimeMillis();
		agregarOperacionSR("usuariosActivos", inicio, fin);
		
		return usuariosActivosPorGenero;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		// TODO Auto-generated method stub
		long inicio = -System.currentTimeMillis();
		ListaDobleEncadenada<VOUsuario> listaUsuarios = usuarios;
		ComparatorVOUsuarioPorTimestampNumeroRatingsId comparador = new ComparatorVOUsuarioPorTimestampNumeroRatingsId();
		listaUsuarios.sort(comparador);
		long fin = System.currentTimeMillis();
		agregarOperacionSR("CatalogoUsuariosOrdenado", inicio, fin);
		return listaUsuarios;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		// TODO Auto-generated method stub
		long incio = -System.currentTimeMillis();
		ListaDobleEncadenada<VOGeneroPelicula> retorno = (ListaDobleEncadenada<VOGeneroPelicula>) GeneroPeliculaPorCriterio(new ComparadorVOPeliculaPorCantidadTags(), n);
		long fin = System.currentTimeMillis();
		agregarOperacionSR("recomendarTagsGeneros", incio, fin);
		
		return retorno; 
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		ListaDobleEncadenada<VOUsuarioGenero> listaDeTagsDeUsuario = new ListaDobleEncadenada<>();

		//Ordenar tags por IdUsuario y por IdPelicula
		tags.sort(new ComparatorVOTagIdUsuarioIdPelicula());
		listaPeliculas.sort(new ComparatorVOPeliculaPorIDPelicula());

		tags.restaurarActual();
		long idUserPrimeraAparicion = tags.darElementoPosicionActual().getIdUsuario();
		VOTag tagActual = tags.darElementoPosicionActual();

		boolean primeraVez = true;

		while(tags.avanzarSiguientePosicion()){
			if(primeraVez){
				primeraVez = false;
				tags.retrocederPosicionAnterior();
			}
			tagActual = tags.darElementoPosicionActual();
			VOUsuarioGenero tagsPorGeneroDeUsuario = new VOUsuarioGenero();
			if(idUserPrimeraAparicion == tagActual.getIdUsuario())
			{
				
				tagsPorGeneroDeUsuario.setIdUsuario(idUserPrimeraAparicion);
				tagsPorGeneroDeUsuario.setListaGeneroTags(new ListaDobleEncadenada<VOGeneroTag>());;

				listaPeliculas.restaurarActual();
				VOPelicula peliculaActual = listaPeliculas.darElementoPosicionActual();
				while(peliculaActual.getIdUsuario() != tagActual.getIdPelicula() && listaPeliculas.avanzarSiguientePosicion())
					peliculaActual = listaPeliculas.darElementoPosicionActual();


				if(peliculaActual.getIdUsuario() == tagActual.getIdPelicula()){
					ListaDobleEncadenada<String> generos = (ListaDobleEncadenada<String>)peliculaActual.getGenerosAsociados();
					generos.sort(new ComparatorString());
					for(String genero: generos){
						ListaDobleEncadenada<VOGeneroTag> lista = (ListaDobleEncadenada<VOGeneroTag>) tagsPorGeneroDeUsuario.getListaGeneroTags(); 
						lista.restaurarActual();
						
						VOGeneroTag existente = lista.darElementoPosicionActual();
						while(!existente.getGenero().equals(genero) && lista.avanzarSiguientePosicion())
							existente = lista.darElementoPosicionActual();
						
						if(existente.getGenero().equals(genero)){
							existente.getTags().agregarElementoFinal(tagActual.getTag());
						}
						
						else{
							VOGeneroTag generoTag = new VOGeneroTag();
							generoTag.setGenero(genero);
							generoTag.setTags(new ListaDobleEncadenada<String>());
							generoTag.getTags().agregarElementoFinal(tagActual.getTag());
						}
					}
				}
			}
			
			else{
				listaDeTagsDeUsuario.restaurarActual();
				VOUsuarioGenero usuarioGeneroAct = listaDeTagsDeUsuario.darElementoPosicionActual();
				while(usuarioGeneroAct.getIdUsuario() != tagsPorGeneroDeUsuario.getIdUsuario() && listaDeTagsDeUsuario.avanzarSiguientePosicion()){
					usuarioGeneroAct = listaDeTagsDeUsuario.darElementoPosicionActual();
				}
				
				if(usuarioGeneroAct.getIdUsuario() == tagsPorGeneroDeUsuario.getIdUsuario()){
				
				}
			}
		}

		return listaDeTagsDeUsuario;
	}


	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		long inicio = -System.currentTimeMillis();
		ListaDobleEncadenada<VOPeliculaUsuario> usuariosRecomendados = new ListaDobleEncadenada<>();
		try{

			File archivoRecomendaciones = new File(rutaRecomendacion);
			BufferedReader lector = new BufferedReader(new FileReader(archivoRecomendaciones));

			String linea = lector.readLine();
			Queue<VORating> cola = new Queue<>();
			ListaDobleEncadenada<VORating> peliculasAProcesar = new ListaDobleEncadenada<>();

			//Cargar las peliculas del usuario con su calificación
			while(linea != null)
			{
				String[] info = linea.split(",");
				long idPelicula = Long.parseLong(info[0].trim());
				double rating = Double.parseDouble(info[1].trim());

				VORating peliculaAProcesar = new VORating();
				peliculaAProcesar.setIdPelicula(idPelicula);
				peliculaAProcesar.setRating(rating);
				peliculasAProcesar.agregarElementoFinal(peliculaAProcesar);

				linea =  lector.readLine();


			}

			//Ordenar las peliculas suministradas por el usuario por IdPelicula
			peliculasAProcesar.sort(new ComparatorVORatingsPorIdPeliculas());


			//Pasar las peliculasAProcesar a una cola
			for(VORating peli: peliculasAProcesar)
				cola.enqueue(peli);


			//Ordenar lista de ratings por IdPelicula

			ratings.sort(new ComparatorVORatingPorIdPeliculaIdUser());

			usuarios.sort(new ComparatorVOUsuarioPorIdPelicula());


			//Restaura el actual en peliculas para ordenar
			ratings.restaurarActual();

			//Procesar cada pelicula suministrada por el usuario

			while(!cola.isEmpty()){

				VORating peliculaActual = cola.dequeue();
				VOPeliculaUsuario usuariosRecomendadosPorPelicula = new VOPeliculaUsuario();
				usuariosRecomendadosPorPelicula.setIdPelicula(peliculaActual.getIdPelicula());
				usuariosRecomendadosPorPelicula.setUsuariosRecomendados(new ListaDobleEncadenada<VOUsuario>());

				VORating ratingActual = ratings.darElementoPosicionActual();

				while(peliculaActual.getIdPelicula() != ratingActual.getIdPelicula() && ratings.avanzarSiguientePosicion())
					ratingActual = ratings.darElementoPosicionActual();

				while(ratingActual.getIdPelicula() == peliculaActual.getIdPelicula()){

					long idUsuario = ratingActual.getIdUsuario();
					VOUsuario usuarioActual = usuarios.darElementoPosicionActual();
					while(usuarioActual.getIdUsuario() != idUsuario && usuarios.avanzarSiguientePosicion())
						usuarioActual = usuarios.darElementoPosicionActual();

					if(usuarioActual.getIdUsuario() == idUsuario)
					{
						double diferenciaAbsoluta = Math.abs(ratingActual.getRating() - peliculaActual.getRating());
						if(diferenciaAbsoluta <= 0.5){
							usuarioActual.setDiferenciaOpinion(Math.abs(ratingActual.getRating() - peliculaActual.getRating()));
							usuariosRecomendadosPorPelicula.getUsuariosRecomendados().agregarElementoFinal(usuarioActual);
						}
					}


					ratings.avanzarSiguientePosicion();
					ratingActual = ratings.darElementoPosicionActual();


				}

				if(ratingActual.getIdPelicula() != peliculaActual.getIdPelicula()){
					//Ordenar listas


					//Reiniciar elementos y agregarlos
					usuarios.restaurarActual();
					usuariosRecomendados.agregarElementoFinal(usuariosRecomendadosPorPelicula);
					ratings.retrocederPosicionAnterior();

				}	
			}

			for(VOPeliculaUsuario peliculaUsuario : usuariosRecomendados){
				ListaDobleEncadenada<VOUsuario> listas = (ListaDobleEncadenada<VOUsuario>) peliculaUsuario.getUsuariosRecomendados();
				listas.sort(new ComparatorVOUsuarioPorDiferenciaOpinion());

				int limite = 0;
				Iterator it = listas.iterator();
				ListaDobleEncadenada<VOUsuario> listaNueva = new ListaDobleEncadenada<>();

				while(it.hasNext() && limite < n){
					VOUsuario user = (VOUsuario) it.next();	
					listaNueva.agregarElementoFinal(user);
					limite++;
				}

				peliculaUsuario.setUsuariosRecomendados(listaNueva);
			}



		}

		catch(Exception e){
			e.printStackTrace();
		}
		
		long fin = System.currentTimeMillis();
		agregarOperacionSR("Recomendar usuarios", inicio, fin);
		return usuariosRecomendados;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		//Organizar tags
		long inicio = System.currentTimeMillis();
		tags.sort(new ComparatorVOTagsPorIDPelicula());
		System.out.println("Hola");
		ListaDobleEncadenada<VOTag> listaDeTags = new ListaDobleEncadenada<>();

		tags.restaurarActual();

		VOTag tagActual = tags.darElementoPosicionActual();
		while(tagActual.getIdPelicula() != idPelicula && tags.avanzarSiguientePosicion())
			tagActual = tags.darElementoPosicionActual();

		boolean masTags = true;

		while(masTags){
			tagActual = tags.darElementoPosicionActual();
			if(tagActual.getIdPelicula() == idPelicula){
				listaDeTags.agregarElementoFinal(tagActual);
				tags.avanzarSiguientePosicion();
			}
			else
				masTags = false;
		}
		long fin = System.currentTimeMillis();
		agregarOperacionSR("tagsPelicula", inicio, fin);
		return listaDeTags;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		// TODO Auto-generated method stub
		VOOperacion op = new VOOperacion();
		op.setOperacion(nomOperacion);
		op.setTimestampFin(tfin);
		op.setTimestampInicio(tinicio);
		operaciones.agregarElementoFinal(op);
	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		Stack<VOOperacion> opApiladas = new Stack();
		int i = 0;
		for(VOOperacion o : operaciones){
			opApiladas.push(o);
			i++;
		}
		ListaDobleEncadenada<VOOperacion> opReverso = new ListaDobleEncadenada<>();
		while(i>0){
			opReverso.agregarElementoFinal(opApiladas.pop());
			i--;
		}			
		return opReverso;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		operaciones= new ListaDobleEncadenada<VOOperacion>();
	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {

		Stack<VOOperacion> opApiladas = new Stack<VOOperacion>();
		int i = n;
		Iterator<VOOperacion> ite = operaciones.iterator();
		while(ite.hasNext() && i>0){
			opApiladas.push(ite.next());
			i--;
		}
		i=n;
		ListaDobleEncadenada<VOOperacion> opReverso = new ListaDobleEncadenada<>();
		while(i>0){
			opReverso.agregarElementoFinal(opApiladas.pop());
			i--;
		}			
		return opReverso;

	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		for(int i=1; i<=n;i++)
			operaciones.eliminarUltimoElemento();

	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		long inicio = -System.currentTimeMillis();
		VOPelicula pelicula = new VOPelicula();
		pelicula.setAgnoPublicacion(agno);
		pelicula.setTitulo(titulo);
		
		//Generar lista correspondiente a generos
		ListaDobleEncadenada<String> generosLista = new ListaDobleEncadenada<>();
		for(String genero: generos)
			generosLista.agregarElementoFinal(genero);
		
		pelicula.setGenerosAsociados(generosLista);
		
		long idPelicula = listaPeliculas.darUltimoElemento().getIdUsuario()+1;
		pelicula.setIdUsuario(idPelicula);
		listaPeliculas.agregarElementoFinal(pelicula);
		
		long fin = System.currentTimeMillis();
		agregarOperacionSR("AgregarPelicula", inicio, fin);
		return idPelicula;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		long inicio = -System.currentTimeMillis();
		VORating ratingActual = new VORating();
		ratingActual.setIdUsuario(idUsuario);
		ratingActual.setIdPelicula(idPelicula);
		ratingActual.setRating(rating);
		
		long fin = System.currentTimeMillis();
		agregarOperacionSR("AgregarRAting", inicio, fin);
		ratings.agregarElementoFinal(ratingActual);
		
		for(VOPelicula pelicula: listaPeliculas){
			if(pelicula.getIdUsuario() == idPelicula){
				double sumaPromedio = pelicula.getPromedioRatings() * pelicula.getNumeroRatings();
				pelicula.setNumeroRatings(pelicula.getNumeroRatings()+1);
				
				sumaPromedio += rating;
				pelicula.setPromedioRatings(sumaPromedio/pelicula.getNumeroRatings());
			}
		}
		

	}

		
}
