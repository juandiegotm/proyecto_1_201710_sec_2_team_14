package model.data_structures;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Random;

import model.Comparator.ComparatorString;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private DoubleNode<T> primero;
	private DoubleNode<T> nodoActual;
	private DoubleNode<T> nodoAux;
	private DoubleNode<T> ultimo;
	private BigDecimal tiempo;
	private int tamanio;


	@Override
	public Iterator<T> iterator() {
		MiIterador<T> it = new MiIterador<T>(primero);
		return it;
	}	

	public ListaDobleEncadenada() {
		primero = ultimo = nodoActual = null;
		tamanio=0;
	}
	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		DoubleNode<T> nodoAgregar = new DoubleNode<T>(elem);
		if(primero == null){
			primero = nodoAgregar;
			nodoActual = primero;
			ultimo = primero;
		}

		else{

			ultimo.next = nodoAgregar;
			nodoAgregar.previous = ultimo;
			ultimo = nodoAgregar;

		}
		tamanio++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		DoubleNode<T> actual = primero;
		while(actual != null && pos > 0)
		{
			actual = actual.next;
			pos--;
		}
		if(pos > 0)
			throw new IndexOutOfBoundsException();
		else
			return actual.element;
	}



	@Override
	public int darNumeroElementos() {
		return tamanio;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return nodoActual.element;
	}

	public T darUltimoElemento(){
		return ultimo.element;
	}
	public T darPrimerElemento(){
		return primero.element;
	}
	public T eliminarUltimoElemento(){

		T elementoEliminado = ultimo.getElement();
		if(primero == ultimo){
			primero = ultimo = null;
		}
		else{
			ultimo.previous.next = null;
			ultimo = ultimo.previous;
			ultimo.next = null;

		}
		tamanio--;
		return elementoEliminado;
	}

	public T eliminarPrimerElemento(){
		T elementoElimiando = primero.element;
		if(primero.next == null)
			primero = null;
		else{
			primero = primero.next;
			primero.previous = null;
		}
		tamanio--;
		return elementoElimiando;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		boolean avanzarSiguiente = false;
		if(nodoActual.next != null){
			nodoActual = nodoActual.next;
			avanzarSiguiente = true;
		}

		return avanzarSiguiente;

	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		boolean retroceder = false;
		if(nodoActual.previous != null){
			nodoActual = nodoActual.previous;
			retroceder = true;
		}


		return retroceder;
	}


	private class DoubleNode <T> {

		private T element;
		private DoubleNode<T> next;
		private DoubleNode<T> previous;

		public DoubleNode (T pElement){
			next = null;
			previous = null;
			element = pElement;
		}

		public T getElement(){
			return element;
		}

		public DoubleNode<T> getNext(){
			return next;
		}

		public DoubleNode<T> getPrevious(){
			return previous;
		}

		public void setNext(DoubleNode<T> pNext){
			next = pNext;
		}

		public void setPrevious(DoubleNode<T> pPrevious){
			previous = pPrevious;
		}

	}

	private class MiIterador<T> implements Iterator<T>{

		DoubleNode<T> actualIt;

		public MiIterador(DoubleNode<T> pPrimero){
			actualIt = pPrimero;
		}

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actualIt != null;
		}
		@Override
		public T next() {
			T element = actualIt.getElement();
			actualIt = actualIt.next;
			return element;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}

	}


	public void shuffle() {
		// TODO Auto-generated method stub

	}

	public void intercambiar(DoubleNode<T> a, DoubleNode<T> b) {
		T aux = a.element;
		a.element = b.element;
		b.element = aux;

	}

	//Ordenamiento

	public void sort (Comparator<T> criterio){
		if(primero != null){
			shuffle();
			iniciarCronometro();
			sortAux(primero, ultimo, criterio);
			pararCronometro();
		}
	}

	private void sortAux(DoubleNode<T> izq, DoubleNode<T> der, Comparator<T> criterio){
		DoubleNode<T> pivot = izq;
		DoubleNode<T> inicial = izq;
		DoubleNode<T> fin = der;

		for(;der.next != izq; ){
			//Si es menor que el pivote, intercambia el izquierdo con el pivote

			if(criterio.compare(izq.element, pivot.element) < 0){
				intercambiar(izq, pivot);
				pivot = pivot.next;
				izq = izq.next;
			}
			//Si el pivote es igual al izquierdo avanza el indice del izquierdo
			else if(criterio.compare(izq.element, pivot.element) == 0){
				izq = izq.next;

			}
			//Si es mayor, se avanza con el indice del derecho.
			else{

				for(boolean seEncontroUnMenor = false;!seEncontroUnMenor && der.previous != null && der.next != izq; der = der.previous){
					//En caso contrario, se intercambia el derecho con el izquierdo
					if(criterio.compare(der.element, pivot.element) < 0){
						seEncontroUnMenor = true;	
						intercambiar(izq, der);

					}
				}
			}
		}



		//Por medio de recursividad de recorren las mitades del arreglo para ordenarlo

		if( pivot.previous != null && inicial!= null && inicial != pivot)
			sortAux(inicial, pivot.previous, criterio);


		if( izq != null && izq.next != null && fin!= null && fin.next != izq )
			sortAux(izq, fin, criterio);



	}

	public boolean isEmpty(){
		return primero == null;
	}

	public void restaurarActual(){
		nodoActual = primero;
	}
	
	public void iniciarNodoAux(){
		nodoAux=nodoActual;
	}
	public boolean nAuxAvanzar(){
		
		boolean avanzarSiguiente = false;
		if(nodoAux.next != null){
			nodoAux = nodoAux.next;
			avanzarSiguiente = true;
		}

		return avanzarSiguiente;
	}
	public boolean nAuxRetrocerer(){
		boolean retroceder = false;
		if(nodoAux.previous != null){
			nodoAux = nodoAux.previous;
			retroceder = true;
		}


		return retroceder;
	}
	public T darElementoAux(){
		return nodoAux.element;
	}
	/**
	 * Retorna una sublista delimitada por el nodo actual y el nodoAux
	 * @return sublista: nodoActual--*>nodoAux
	 */
	public ListaDobleEncadenada<T> subListaAux(){
		ListaDobleEncadenada<T> rta = new ListaDobleEncadenada<>();
		rta.primero=nodoActual;
		rta.ultimo=nodoAux;
		rta.ultimo.next=null;
		rta.primero.previous=null;
		return rta;
	}

	public void sortSinRep(Comparator<T> criterio){

		shuffle();
		iniciarCronometro();
		sortAux(primero, ultimo, criterio);
		DoubleNode<T> act = primero;
		DoubleNode<T> primeraRepeticion = primero;
		while(act.next != null){
			if(criterio.compare(act.element, primeraRepeticion.element) != 0){
				primeraRepeticion.next = act;
				act.previous = primeraRepeticion;
				primeraRepeticion = act;
			}	
			act = act.next;
		}
		primeraRepeticion.next = null;
		act.previous = primeraRepeticion;
		pararCronometro();

	}
	private void sortAuxSinRep(DoubleNode<T> izq, DoubleNode<T> der, Comparator<T> criterio){
		DoubleNode<T> pivot = izq;
		DoubleNode<T> inicial = izq;
		DoubleNode<T> fin = der;

		for(; izq.next != null && izq != der && der.next != izq; ){
			//Si es menor que el pivote, intercambia el izquierdo con el pivote

			if(criterio.compare(izq.element, pivot.element) < 0){
				intercambiar(izq, pivot);
				pivot = pivot.next;
				izq = izq.next;
			}
			//Si el pivote es igual al izquierdo avanza el indice del izquierdo
			else if(criterio.compare(izq.element, pivot.element) == 0){
				izq = izq.next;
				pivot.next=izq;
				izq.previous=pivot;
			}
			//Si es mayor, se avanza con el indice del derecho.
			else{

				for(boolean seEncontroUnMenor = false;!seEncontroUnMenor && der.previous != null && der.next != izq; der = der.previous){
					//En caso contrario, se intercambia el derecho con el izquierdo
					if(criterio.compare(der.element, pivot.element) < 0){
						seEncontroUnMenor = true;	
						intercambiar(izq, der);

					}
				}
			}
		}



		//Por medio de recursividad de recorren las mitades del arreglo para ordenarlo

		if(pivot.previous != null && inicial!= null && inicial != pivot)
			sortAuxSinRep(inicial, pivot.previous, criterio);


		if( izq.previous != null && fin!= null && izq !=fin )
			sortAuxSinRep(izq, fin, criterio);

	}
	private void eliminarRepetidos(Comparator criterio, DoubleNode<T> primero) {
		if(primero.next!=null){
			DoubleNode<T> piv = primero;
			DoubleNode<T> der = piv.next;
			while(criterio.compare(piv.element, der.element)==0 && der.next!=null)
				der=der.next;

			piv.next=der;
			der.previous=piv;
			if(der.next==null){
				if(criterio.compare(piv.element, der.element)==0)
					eliminarUltimoElemento();
			}
			else
				eliminarRepetidos(criterio, der);
		}
	}




	private void iniciarCronometro(){
		tiempo = new BigDecimal(-System.currentTimeMillis() + "");
	}

	private void pararCronometro(){
		tiempo = tiempo.add(new BigDecimal(System.currentTimeMillis() + ""));
	}

	public BigDecimal darTiempo(){

		return tiempo;
	}

	public static void main(String[] args) {
		ListaDobleEncadenada<Integer> numeros = new ListaDobleEncadenada<>();
		int tamanio = 10;
		int limite = 10;
		Random r =  new Random();

		for(int i = 0; i < tamanio; i++ )
			numeros.agregarElementoFinal(r.nextInt(limite));

		System.out.println("Empiezo a ordenar");
		for(Integer num: numeros)
			System.out.print(" " + num);

		System.out.println();
		numeros.sort(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				if(o1 > o2)
					return 1;
				else if(o2 > o1)
					return -1;
				else
					return 0;
			}
		});
		for(Integer num: numeros)
			System.out.print(" " + num);

		System.out.println(" Tiempo: " + numeros.darTiempo());
	}

	public void eliminarRepetidos(Comparator<T> criterio) {
		eliminarRepetidos(criterio, primero);

	}


}

