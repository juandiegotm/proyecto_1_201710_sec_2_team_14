package model.data_structures;

public class Stack <T> implements IStack<T> {

	private ListaDobleEncadenada<T> lista;

	public Stack(){
		lista = new ListaDobleEncadenada<>();
	}

	public void push(T item) {
		lista.agregarElementoFinal(item);
	}

	@Override
	public T pop() {
		return lista.eliminarUltimoElemento();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos() == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos();
	}



}
