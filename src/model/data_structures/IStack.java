package model.data_structures;

public interface IStack <T> {

	/**
	 * Agrega un Item al tope de la pila.
	 * @param item
	 */
	void push(T item);
	
	
	/**
	 * Elimina el Item que est� en el tope de la pila
	 * @return el elemento T que elimina del tope de la pila.
	 */
	T pop();
	
	
	/**
	 * Indica si la pila est� vac�a
	 * @return true si la pila est� vac�a, false en caso contrario.
	 */
	boolean isEmpty();
	
	
	/**
	 * Returna el numero de elementos en la pila.
	 * @return n�mero de elementos en la pila.
	 */
	int size();
}
