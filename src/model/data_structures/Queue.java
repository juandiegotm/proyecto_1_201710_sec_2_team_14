package model.data_structures;

public class Queue <T> implements IQueue<T>{

	ListaDobleEncadenada<T> lista = new ListaDobleEncadenada<>();
	
	@Override
	public void enqueue(T element) {
		lista.agregarElementoFinal(element);
		
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		return lista.eliminarPrimerElemento();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos() == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos();
	}
	
	public static void main(String[] args) {
		Queue<Integer> fila = new Queue<>();
		fila.enqueue(2);
		
		System.out.println(fila.dequeue());
	}

}
