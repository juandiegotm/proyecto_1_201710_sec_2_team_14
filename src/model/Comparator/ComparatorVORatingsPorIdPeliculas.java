package model.Comparator;

import java.util.Comparator;

import model.vo.VORating;

public class ComparatorVORatingsPorIdPeliculas implements Comparator<VORating>{

	@Override
	public int compare(VORating o1, VORating o2) {
		if(o1.getIdPelicula() > o2.getIdPelicula())
			return 1;
		else if(o1.getIdPelicula() < o2.getIdPelicula())
			return -1;

		return 0;
	}

}
