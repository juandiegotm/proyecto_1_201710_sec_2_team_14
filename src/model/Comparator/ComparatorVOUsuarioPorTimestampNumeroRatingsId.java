package model.Comparator;
import java.util.Comparator;

import model.vo.VOUsuario;

public class ComparatorVOUsuarioPorTimestampNumeroRatingsId implements Comparator<VOUsuario>
{

	@Override
	public int compare(VOUsuario o1, VOUsuario o2) {
		if(o1.getPrimerTimestamp() > o2.getPrimerTimestamp())
			return 1;
		
		else if(o1.getPrimerTimestamp() < o2.getPrimerTimestamp())
			return -1;
		
		else{
			if(o1.getNumRatings() > o2.getNumRatings())
				return 1;
			
			else if(o1.getNumRatings() < o2.getNumRatings())
				return -1;
			
			else{
				if(o1.getIdUsuario() > o2.getIdUsuario())
					return 1;
				else if(o1.getIdUsuario() < o2.getIdUsuario())
					return -1;
				else
					return 0;
			}
		}
	}

}
