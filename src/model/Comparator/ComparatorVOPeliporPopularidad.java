package model.Comparator;

import java.util.Comparator;

import model.vo.VOPelicula;

public class ComparatorVOPeliporPopularidad implements Comparator<VOPelicula>{

	@Override
	public int compare(VOPelicula p1, VOPelicula p2) {
		int i = 0;
		if(p1.getNumeroRatings()>p2.getNumeroRatings())
			i=-1;
		if(p1.getNumeroRatings()<p2.getNumeroRatings())
			i=1;
		return i;
	}
}
