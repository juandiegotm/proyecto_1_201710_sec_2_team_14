package model.Comparator;

import java.util.Comparator;

import model.vo.VOPelicula;

public class ComparatorVOPeliculaPorPromCalificacion implements Comparator<VOPelicula>{

	@Override
	public int compare(VOPelicula a, VOPelicula b) {
		int i=0;
		if(a.getPromedioRatings()>b.getPromedioRatings())
			i=-1;
		if(a.getPromedioRatings()<b.getPromedioRatings())
			i=1;
		return 1;
	}

}
