package model.Comparator;

import java.util.Comparator;

import model.vo.VOPelicula;

public class ComparatorVOPeliculasGeneral implements Comparator<VOPelicula>{

	public int compare(VOPelicula p0, VOPelicula p1) {
		int i=-1;
		
		if(p0.getAgnoPublicacion()==p1.getAgnoPublicacion()){
			if(p0.getPromedioRatings()==p1.getPromedioRatings())
				i=(new ComparatorString()).compare(p0.getTitulo(), p1.getTitulo());
			else if(p0.getPromedioRatings()>p1.getPromedioRatings())
				i=1;
			else{}
		}
		else if(p0.getAgnoPublicacion()<p1.getAgnoPublicacion())
			i=1;
		else{}
		
		return i;
	}

}