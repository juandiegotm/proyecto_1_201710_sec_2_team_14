package model.Comparator;
import java.util.Comparator;

import model.vo.VOTagExtendido;

public class ComparatorVOTagsExtendidoPorIDPelicula implements Comparator<VOTagExtendido> {

	@Override
	public int compare(VOTagExtendido o1, VOTagExtendido o2) {
		// TODO Auto-generated method stub
		return Long.compare(o1.getIdPelicula(), o2.getIdPelicula());
	}

}
