package model.Comparator;

import java.util.Comparator;

import model.vo.VOUsuarioConteo;

public class ComparatorVOUsuarioConteoConteo implements Comparator<VOUsuarioConteo> {

	@Override
	public int compare(VOUsuarioConteo o1, VOUsuarioConteo o2) {
		if( o1.getConteo() < o2.getConteo())
			return 1;
		
		
		else if(o2.getConteo() < o1.getConteo())
			return -1;
		
		else
			return 0;
	}

}
