package model.Comparator;

import java.util.Comparator;

import model.vo.VOPelicula;

public class ComparatorVOPeliculaPorAgno implements Comparator<VOPelicula>{

	@Override
	public int compare(VOPelicula o1, VOPelicula o2) {
		int i = 0;
			if(o1.getAgnoPublicacion()>o2.getAgnoPublicacion())
				i=-1;
			else if(o1.getAgnoPublicacion()<o2.getAgnoPublicacion())
				i=1;
		return i;
	}

}
