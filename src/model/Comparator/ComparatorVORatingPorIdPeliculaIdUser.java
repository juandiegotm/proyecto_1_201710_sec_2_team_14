package model.Comparator;
import java.util.Comparator;

import model.vo.VORating;

public class ComparatorVORatingPorIdPeliculaIdUser  implements Comparator<VORating>{
	public int compare(VORating o1, VORating o2) {
		// TODO Auto-generated method stub
		if(o1.getIdPelicula() > o2.getIdPelicula())
			return 1;
		
		else if(o1.getIdPelicula() < o2.getIdPelicula())
			return -1;
		
		else{
			if(o1.getIdUsuario() > o2.getIdUsuario())
				return 1;
			
			else if(o1.getIdUsuario() <o2.getIdUsuario())
				return -1;
			
			else
				return 0;
						
		}
	}
}
