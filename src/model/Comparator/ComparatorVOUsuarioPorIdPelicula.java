package model.Comparator;

import java.util.Comparator;

import model.vo.VOUsuario;


public class ComparatorVOUsuarioPorIdPelicula implements Comparator<VOUsuario> {

	public int compare(VOUsuario o1, VOUsuario o2) {
		if(o1.getIdUsuario() > o2.getIdUsuario())
			return 1;

		else if(o1.getIdUsuario() < o2.getIdUsuario())
			return -1;

		else
			return 0;
		
	}
}