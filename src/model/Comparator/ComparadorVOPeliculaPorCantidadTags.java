package model.Comparator;

import java.util.Comparator;

import model.vo.VOPelicula;

public class ComparadorVOPeliculaPorCantidadTags implements Comparator<VOPelicula> {

	@Override
	public int compare(VOPelicula o1, VOPelicula o2) {
		return   o2.getNumeroTags() - o1.getNumeroTags();
		
	}

}
