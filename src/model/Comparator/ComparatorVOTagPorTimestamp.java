package model.Comparator;
import java.util.Comparator;
import model.vo.VOTag;

public class ComparatorVOTagPorTimestamp implements Comparator<VOTag> {

	@Override
	public int compare(VOTag o1, VOTag o2) {
		if(o1.getTimestamp() < o2.getTimestamp())
			return 1;
		else if(o2.getTimestamp() < o1.getTimestamp())
			return -1;
		
		else 
			return 0;
	}

}
