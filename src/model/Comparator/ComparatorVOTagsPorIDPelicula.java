package model.Comparator;
import java.util.Comparator;

import model.vo.VOTag;

public class ComparatorVOTagsPorIDPelicula implements Comparator<VOTag> {

	@Override
	public int compare(VOTag o1, VOTag o2) {
		// TODO Auto-generated method stub
		return Long.compare(o1.getIdPelicula(), o2.getIdPelicula());
	}

}
