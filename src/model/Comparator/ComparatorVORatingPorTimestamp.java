package model.Comparator;

import java.util.Comparator;

import model.vo.VORating;

public class ComparatorVORatingPorTimestamp implements Comparator<VORating>{

	@Override
	public int compare(VORating o1, VORating o2) {
		if(o1.getTimestamp() > o2.getTimestamp())
			return -1;
		else if(o1.getTimestamp() < o2.getTimestamp())
			return 1;

		return 0;
	}

}
