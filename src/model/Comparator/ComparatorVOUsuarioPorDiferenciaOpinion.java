package model.Comparator;
import model.vo.VOUsuario;
import java.util.Comparator;

public class ComparatorVOUsuarioPorDiferenciaOpinion implements Comparator<VOUsuario> {
	@Override
	public int compare(VOUsuario o1, VOUsuario o2) {
		// TODO Auto-generated method stub
		if(o1.getDiferenciaOpinion() < o2.getDiferenciaOpinion())
			return 1;

		else if(o1.getDiferenciaOpinion() > o2.getDiferenciaOpinion())
			return -1;

		else
			return 0;
	}

}
