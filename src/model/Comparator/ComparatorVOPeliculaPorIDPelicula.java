package model.Comparator;

import java.util.Comparator;

import model.vo.VOPelicula;

public class ComparatorVOPeliculaPorIDPelicula implements Comparator<VOPelicula>{

	@Override
	public int compare(VOPelicula o1, VOPelicula o2) {
		if(o1.getIdUsuario() > o2.getIdUsuario())
			return 1;
		
		else if(o1.getIdUsuario() < o2.getIdUsuario())
			return -1;
		
		else
			return 0;
				
	}

}
