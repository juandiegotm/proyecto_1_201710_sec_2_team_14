package model.Comparator;
import java.util.Comparator;

import model.vo.VOPelicula;
import model.vo.VOTag;

public class ComparatorVOTagIdUsuarioIdPelicula  implements Comparator<VOTag>{

	@Override
	public int compare(VOTag o1, VOTag o2) {
		// TODO Auto-generated method stub
		if(o1.getIdUsuario() > o2.getIdUsuario())
			return 1;

		else if(o1.getIdUsuario() < o2.getIdUsuario())
			return -1;

		else{
			if(o1.getIdPelicula() > o2.getIdPelicula())
				return 1;

			else if(o1.getIdPelicula() < o2.getIdPelicula())
				return -1;
			
			else{
				return 0;
			}
		}
	}

}
