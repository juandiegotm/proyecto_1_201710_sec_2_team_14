package model.vo;

public class VORating {
	
	private long idUsuario;
	private long idPelicula;
	private double rating;
	private long timestamp;
	
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public void setTimestamp(long time){
		this.timestamp=time;
	}
	public long getTimestamp(){
		return timestamp;
	}
	

}
